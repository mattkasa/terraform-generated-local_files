#!/bin/bash

for ((i=1;i<=${1:-10};i++)); do
  sed -e "s/<%= @id %>/${i}/g" template.tf.erb
done > main.tf
